import React from 'react';
import TinyMCE from './WebContentEditor.jsx'

class TinyMCEPDF extends React.Component {
  constructor(props) {
    super(props);
    this.toggleModal = this.toggleModal.bind(this);
    this.setEditor = this.setEditor.bind(this);
    this.state = {
      "showModal": false,
    };
  }
  componentWillMount(){
    this.setState({
      "showModal" : false,
    })
  }
  // componentDidMount(){tinymce.get
  //   this.setState({
  //     "previewData" : tinymce.get("tinymce").getContent()
  //   })
  // }
 toggleModal(){
   if(this.state.showModal){
     this.setState({"showModal":false})
   }else{
     this.setState({"showModal":true})
   }
 }

 setEditor(editor){
   this.setState({"editor":editor})
 }
  render() {
 var JSONDataComplex = this.props.variableJSON;
 var contentData = this.props.contentHTML;
 var pdfUploadURL = this.props.pdfUploadURL;
  var showModal = this.state.showModal;
  var modalContent = this.state.modalContent;
  var editor = this.state.editor;

    return (
        <TinyMCE showModal={showModal}  toggleModal={this.toggleModal}  variableJSON={JSONDataComplex} contentHTML={contentData} saveTemplate={this.props.saveTemplate} overloadTemplate={this.props.overloadTemplate} closeBtn={this.props.closeBtn} pdfUploadURL={pdfUploadURL} uploadPath={this.props.uploadPath} printCallbackFunction={this.props.printCallbackFunction}></TinyMCE>
    );
  }
}

export default TinyMCEPDF;
