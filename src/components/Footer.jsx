import React from 'react';

class Footer extends React.Component {
  render() {
    return (
      <div className="footer">
        <div onClick={this.props.printFunction}>
          <div className="previsualizarBtn pull-left">
            <img className="tinymceFooterIcon" src="/node_modules/webcontenteditor/lib/img/eye.png"></img>PREVISUALIZAR
          </div>
        </div>
        <div onClick={this.props.saveTemplate}>
          <div className="guardarBtn">
            <img className="tinymceFooterIcon" src="/node_modules/webcontenteditor/lib/img/check.png"></img>
            GUARDAR COMO NUEVA PLANTILLA</div>
        </div>
        <div onClick={this.props.overloadTemplate}>
          <div className="previsualizarBtn pull-right">
            <img className="tinymceFooterIcon" src="/node_modules/webcontenteditor/lib/img/check.png"></img>
            SOBREESCRIBIR PLANTILLA</div>
        </div>
      </div>
    )
  }
}
export default Footer;
