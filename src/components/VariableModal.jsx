import React from 'react';

class VariableModal extends React.Component {
  render() {
    return(
      <div className="modal fade" id="variableModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h2 className="modal-title" id="myModalLabel">Variables</h2>
            </div>
            <div className="modal-body"></div>
          </div>
        </div>
      </div>
    )
  }
}
export default VariableModal;
