import React from 'react';
var request = require('superagent');

class WebContentPreview extends React.Component {
  componentDidMount() {
    tinymce.init({
      selector: '#tinymcePreview',
      height: 500,
      theme: 'modern',
      plugins: [
        "contentData advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu imagetools directionality", "emoticons template paste textcolor colorpicker textpattern code"
      ],
      toolbar1: "print preview | insertfile undo redo  | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code | help  | contentData",
      file_picker_types: 'file image media',
      /*
      Incase the image "src" has to be replaced with the remote server upload path
      Image Upload handler should return a JSON {location:"absolute_path"}
      eg : {location:"http://cdnserver/sampleimage.png"}
      */
      images_upload_handler: function(blobInfo, success, failure) {
        var formData;
        formData = new FormData();
        formData.append('upload', blobInfo.blob(), blobInfo.filename());
        request.post('https://apps.anfix.com/os/media/upload')
        // .set('Cookie','accessCode=XXXXXXX')
          .withCredentials().timeout(10000).send(formData). // query string
        end(function(err, res) {
          if (err != null) {
            failure('Error: ' + err.message);
            return;
          }
          if (res.statusCode != 200) {
            failure('HTTP Error: ' + res.statusText);
            return;
          }
          var responseData = JSON.parse(res.text);
          if (responseData.result != 0) {
            failure(responseData.errorList[0].text);
            return;
          }
          var URL = "https://apps.anfix.com/os/media/download?uid=" + responseData.outputData.Media[0].UID;
          success(URL)
        });
      },
      file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function() {
            var id = 'IMG' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);
            cb(blobInfo.blobUri(), {title: file.name});
          };
        };
        input.click();
      },
      automatic_uploads: true,
      data: this.props.JSONData
    });
  }
  componentWillReceiveProps(nextProps) {
    console.log(nextProps.contentPreviewData)
    var jsonContent = {};
    var jsonData = nextProps.JSONData;
    jsonData.map(function(data) {
      if (data.tagType == 1) {
        if (typeof data.value == "string") {
          jsonContent[data.tag] = data.value
        } else if (typeof data.value == "object") {
          this.objectCreator(data, jsonContent)
        }
      }
    })
    var torender = Mustache.render(nextProps.contentPreviewData, jsonContent);
    tinymce.get("tinymcePreview").setContent(torender)
  }
  objectCreator(data, content) {
    var temp = {}
    data.value.map(function(inner) {
      if (typeof inner.value == "object") {
        objectCreator(inner, temp)
      } else
        temp[inner.tag] = inner.value;
      }
    )
    content[data.tag] = temp;
  }
  render() {
    return (
      <div>
        <h1>Preview</h1>
        <textarea id="tinymcePreview">{this.props.contentPreviewData}</textarea>
      </div>
    );
  }
}

export default WebContentPreview;
