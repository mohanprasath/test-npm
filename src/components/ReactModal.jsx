import React from 'react';

class ReactModal extends React.Component {
  render() {
      if (this.props.showModal) {
          return this.props.children;
      }
      else {
          return false;
      }
  }
}
export default ReactModal;
