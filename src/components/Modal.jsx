import React from 'react';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.buildCSS = this.buildCSS.bind(this);
    this.buildSpanString = this.buildSpanString.bind(this);
  }
  buildCSS(css) {
    if (css == undefined) {
      return ";"
    }
    var cssString = "padding: 7px;";
    var keys = Object.keys(css);
    keys.map(function(key) {
      cssString = key + ":" + css[key] + ";" + cssString
    })
    return cssString;
  }
  buildSpanString(td) {
    var spanString = " ";
    if (td.colspan != undefined) {
      spanString = spanString + "colspan=" + td.colspan;
    }
    if (td.rowspan != undefined) {
      spanString = spanString + "rowspan=" + td.rowspan;
    }
    return spanString + ";";
  }
  render() {
    // Render nothing if the "show" prop is false
    if (!this.props.show) {
      return null;
    }
    var editor = this.props.editor
    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      backgroundColor: 'rgba(0,0,0,0.3)',
      padding: 50
    };

    // The modal "window"
    const modalStyle = {
      backgroundColor: '#fff',
      borderRadius: 5,
      minHeight: 300,
      margin: '0 auto'
    };
    var buildCSS = this.buildCSS;
    var buildSpanString = this.buildSpanString;
    var editor = this.props.editor;
    var dataComplex = this.props.jsonData
    var keys = Object.keys(dataComplex)
    var menuGroupCollector = [];
    close = this.props.onClose;
    keys.map(function(key, i) {
      var menuContent = dataComplex[key].map(function(element, i) {
        if (element.tagType == 1) {
          var insertContent = "{{" + element.tag + "}}"
          return <tr onClick={function() {
            editor.insertContent(insertContent);
            close()
          }}>
            <td><img className='addImage' src='/node_modules/webcontenteditor/lib/img/add.png'/></td>
            <td>{element.label}</td>
          </tr>
        } else if (element.tagType == 2) {
          var insertContent = "<table width=97% cellpadding=7><tbody>"
          var addContent;
          var tableContent = element.value;
          var addContent = tableContent.map(function(row, i) {
            if (i == 0) {
              var tdContent = row.value.map(function(td) {
                var cssString = buildCSS(td.css)
                return "<td style=" + cssString + ">" + td.value + "</td>"
              })
              tdContent = tdContent.join("")
              return "<tr>" + tdContent + "</tr>"
            }
            var tdContent = row.value.map(function(td) {
              var cssString = buildCSS(td.css)
              return "<td style=" + cssString + ">" + td.value + "</td>"
            })
            tdContent = tdContent.join("")
            return "<tr>" + tdContent + "</tr>"
          })
          addContent = addContent.join("")
          insertContent = insertContent + addContent + "</tbody></table>"
          return <tr onClick={function() {
            editor.insertContent(insertContent);
            close()
          }}>
            <td><img className='addImage' src='/node_modules/webcontenteditor/lib/img/add.png'/></td>
            <td>{element.label}</td>
          </tr>
        } else if (element.tagType == 3) {
          var insertContent = "<table width=97% cellpadding=7><tbody>"
          var addContent;
          var tableContent = element.value;
          var addContent = tableContent.map(function(row, i) {
            if (i == 0) {
              var tdContent = row.value.map(function(td) {
                var cssString = buildCSS(td.css)
                var spanString = buildSpanString(td)
                return "<td style=" + cssString + spanString + ">" + td.value + "</td>"
              })
              tdContent = tdContent.join("")
              return "<tr>" + tdContent + "</tr>"
            }
            var tdContent = row.value.map(function(td) {
              var cssString = buildCSS(td.css)
              var spanString = buildSpanString(td)
              return "<td style=" + cssString + spanString + ">" + td.value + "</td>"
            })
            tdContent = tdContent.join("")
            return "<tr>" + tdContent + "</tr>"
          })
          addContent = addContent.join("")
          insertContent = insertContent + addContent + "</tbody></table>"
          return <tr onClick={function() {
            editor.insertContent(insertContent);
            close()
          }}>
            <td><img className='addImage' src='/node_modules/webcontenteditor/lib/img/add.png'/></td>
            <td>{element.label}</td>
          </tr>
        }
      })
      var menuContentTable = (
        <table className='variableTable'>
          <tbody>{menuContent}</tbody>
        </table>
      )
      var content = <div className='variableDiv'>
        <h4>{key}</h4>{menuContentTable}</div>
      menuGroupCollector.push({"content": content, "length": menuContent.length});

    })
    menuGroupCollector.sort(function(a, b) {
      if (a.length > b.length)
        return -1;
      if (a.length < b.length)
        return 1;
      return 0;
    })
    var variableDesc = <p style={{
      "font-size": "15px",
      "margin-left": "22px",
      "padding": "18px",
      "background-color": "#dadada;"
    }}>Variable Description
    </p>
    var totalContents = menuGroupCollector.map(function(content) {
      return content.content
    })
    return (
      <div classNameName="backdrop" style={backdropStyle}>
        <div classNameName="modal " id="variableModalDiv" style={modalStyle}>
          <div className="modal fadein" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" onClick={this.props.onClose} data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h2 className="modal-title" id="myModalLabel">Variables</h2>
                </div>
                <div className="modal-body">
                  {variableDesc}
                  {totalContents}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
