import React from 'react';
import Footer from './Footer.jsx'
import Modal from './Modal.jsx'
var request = require('superagent');
import Mustache from 'mustache'

/**
 *
 * @class TinyMCE
 * @extends React.Component
 * @module Core
 */
export default class TinyMCE extends React.Component {
  constructor(props) {
    super(props);
    this.initialiseEditor = this.initialiseEditor.bind(this);
    this.removeEditor = this.removeEditor.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.setEditor = this.setEditor.bind(this);
    this.overloadTemplate = this.overloadTemplate.bind(this);
    this.saveTemplate = this.saveTemplate.bind(this);
    this.objectCreator = this.objectCreator.bind(this);
    this.printwkhtml = this.printwkhtml.bind(this);
    this.printPDF = this.printPDF.bind(this);
    this.request = request;
    this.state = {
      show: false,
      hotReloadPDF: ""
    };
  }
  printPDF(content , dataComplex , uploadPath , pdfUploadURL , callback , request ,parent){
      var contents = content
      var dataComplex = dataComplex
      var uploadPath = uploadPath
      var pdfUploadURL = pdfUploadURL
      var callback = callback
      var request = request
      var jsonContent = {};
      var jsonData = dataComplex;
      var keys = Object.keys(jsonData)
      keys.map(function(key) {
        jsonData[key].map(function(data) {
          if (data.tagType == 1) {
            if (typeof data.value == "string") {
              jsonContent[data.tag] = data.value
            } else if (typeof data.value == "object") {
              this.objectCreator(data, jsonContent)
            }
          }
        })
      })
      var torender = Mustache.render(contents, jsonContent);
      torender = "<html><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/><style>table, tr, td, th, tbody, thead, tfoot {page-break-inside: avoid !important;}.tableTinyMCE{border: 1px solid black;border-collapse: collapse; border-spacing: 0;max-width: 650px;}.tableTinyMCE td{border:1px solid black;}</style>" + torender + "</html>";
      request.post(pdfUploadURL)
      // request.post('http://localhost:80/wkhtmlupload.php')
        .type('form').send({html: torender, hotReloadName: parent.state.hotReloadPDF, uploadPath: uploadPath}).end(function(err, res) {
        if (err || !res.ok) {
          //PDF failed
          var data = res.text;
        } else {
          var data = res.text;
          parent.setState({"hotReloadPDF": data.substr(data.lastIndexOf('/') + 1)});
          callback(contents,jsonData,data);
        }
      })
  }
  toggleModal() {
    this.props.toggleModal()
  }
  setEditor(editor) {
    this.props.setEditor(editor)
  }
  overloadTemplate() {
    var html = this.state.editor.getContent()
    var json = this.props.variableJSON
    var uploadPath = this.props.uploadPath
    var pdfUploadURL = this.props.pdfUploadURL
    var callback = function(url){ return url;}
    var request = this.request
    var pdfurl = this.printPDF(html,json, uploadPath , pdfUploadURL ,this.props.overloadTemplate , request , this )
  }
  saveTemplate() {
    var html = this.state.editor.getContent()
    var json = this.props.variableJSON
    var uploadPath = this.props.uploadPath
    var pdfUploadURL = this.props.pdfUploadURL
    var callback = function(url){ return url;}
    var request = this.request
    var pdfurl = this.printPDF(html,json, uploadPath , pdfUploadURL ,this.props.saveTemplate , request , this )
  }
  componentDidMount() {
    if (typeof window === 'undefined' || typeof document === 'undefined')
      return;
    if (window.tinymce || window._has_requested_tinymce) {
      this.initialiseEditor();
      return;
    }
    this.script = document.createElement('script');
    this.script.type = 'application/javascript';
    this.script.addEventListener('load', this.initialiseEditor);
    this.script.src = `https://cloud.tinymce.com/stable/tinymce.min.js`;
    document.head.appendChild(this.script);
    window._has_requested_tinymce = true;
  }

  componentWillUnmount() {
    if (typeof this.script !== 'undefined') {
      this.script.removeEventListener('load', this.initialiseEditor);
    }

    if (this.state.editor) {
      this.removeEditor();
    }
  }

  // Creating Mustache template's compatible JSON object
  objectCreator(data, content) {
    var temp = {}
    data.value.map(function(inner) {
      if (typeof inner.value == "object") {
        objectCreator(inner, temp)
      } else
        temp[inner.tag] = inner.value;
      }
    )
    content[data.tag] = temp;
  }

  initialiseEditor() {
    if (typeof window === 'undefined')
      return;
    if (window._has_requested_tinymce && !window.tinymce && !this.state.editor) {
      // Multiple editors on the page, one of the other ones has already requested the tinymce script
      setTimeout(() => {
        this.initialiseEditor();
      }, 500);
      return;
    }

    /*Modal Plugin*/
    tinymce.PluginManager.add('contentDataModalComplex', function(editor, url) {
      var dataComplex = editor.getParam("jsonData")

      // Add a button that opens a window
      editor.addButton('contentDataModalComplex', {
        image: '/node_modules/webcontenteditor/lib/img/variable.png',
        id: "tinymceVariableButton",
        cmd: "tinymceVariable",
        tooltip : "insertar variables"
      });

      //Editor commands
      editor.addCommand('tinymceVariable', function() {
        var parentComponent = editor.getParam("masters");
        parentComponent.toggleModal();
        return;
      });
    });

    /*Print Plugin*/
    tinymce.PluginManager.add('printwkhtmlpdf', function(editor, url) {
      editor.addCommand('tinymcewkhtmlPrint', function() {
        var contents = editor.getContent()
        var dataComplex = editor.getParam("jsonData");
        var uploadPath = editor.getParam("uploadPath");
        var pdfUploadURL = editor.getParam("pdfUploadURL")
        var callback = editor.getParam("printCallback");
        var request = editor.getParam("request");
        var masters = editor.getParam("masters")
        var jsonContent = {};
        var jsonData = dataComplex;
        var keys = Object.keys(jsonData)
        keys.map(function(key) {
          jsonData[key].map(function(data) {
            if (data.tagType == 1) {
              if (typeof data.value == "string") {
                jsonContent[data.tag] = data.value
              } else if (typeof data.value == "object") {
                this.objectCreator(data, jsonContent)
              }
            }
          })
        })
        var torender = Mustache.render(editor.getContent(), jsonContent);
        torender = "<html><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/><style>table, tr, td, th, tbody, thead, tfoot {page-break-inside: avoid !important;}.tableTinyMCE{border: 1px solid black;border-collapse: collapse; border-spacing: 0;max-width: 650px;}.tableTinyMCE td{border:1px solid black;}</style>" + torender + "</html>";
        request.post(pdfUploadURL)
        // request.post('http://localhost:80/wkhtmlupload.php')
          .type('form').send({html: torender, hotReloadName: masters.state.hotReloadPDF, uploadPath: uploadPath}).end(function(err, res) {
          if (err || !res.ok) {
            //PDF failed
            var data = res.text;
          } else {
            var data = res.text;
            masters.setState({"hotReloadPDF": data.substr(data.lastIndexOf('/') + 1)});
            callback(data);
          }
        })
      })
      editor.addButton('printwkhtmlpdf', {
        text: "PrintContent-WKHTMLPDF",
        cmd: 'tinymcewkhtmlPrint'
      })
    })

    /* Tinymce Close Plugin */
    tinymce.PluginManager.add('customClose', function(editor, url) {
      var dataComplex = editor.getParam("dataComplex")
      // Add a button that opens a window
      editor.addButton('customClose', {
        image: '/node_modules/webcontenteditor/lib/img/close.png',
        cmd: "customCloseFunction",
        tooltip :"cerca"
      });

      //Editor commands
      editor.addCommand('customCloseFunction', function() {
        var closeFunction = editor.getParam('closeFunc');
        closeFunction();
      });
    });

    tinymce.addI18n('es',{
    "Cut": "Cortar",
    "Heading 5": "Encabezado 5",
    "Header 2": "Encabezado 2 ",
    "Your browser doesn't support direct access to the clipboard. Please use the Ctrl+X\/C\/V keyboard shortcuts instead.": "Tu navegador no soporta acceso directo al portapapeles. Por favor usa las teclas Crtl+X\/C\/V de tu teclado",
    "Heading 4": "Encabezado 4",
    "Div": "Capa",
    "Heading 2": "Encabezado 2",
    "Paste": "Pegar",
    "Close": "Cerrar",
    "Font Family": "Familia de fuentes",
    "Pre": "Pre",
    "Align right": "Alinear a la derecha",
    "New document": "Nuevo documento",
    "Blockquote": "Bloque de cita",
    "Numbered list": "Lista numerada",
    "Heading 1": "Encabezado 1",
    "Headings": "Encabezados",
    "Increase indent": "Incrementar sangr\u00eda",
    "Formats": "Formatos",
    "Headers": "Encabezados",
    "Select all": "Seleccionar todo",
    "Header 3": "Encabezado 3",
    "Blocks": "Bloques",
    "Undo": "Deshacer",
    "Strikethrough": "Tachado",
    "Bullet list": "Lista de vi\u00f1etas",
    "Header 1": "Encabezado 1",
    "Superscript": "Super\u00edndice",
    "Clear formatting": "Limpiar formato",
    "Font Sizes": "Tama\u00f1os de fuente",
    "Subscript": "Sub\u00edndice",
    "Header 6": "Encabezado 6",
    "Redo": "Rehacer",
    "Paragraph": "P\u00e1rrafo",
    "Ok": "Ok",
    "Bold": "Negrita",
    "Code": "C\u00f3digo",
    "Italic": "It\u00e1lica",
    "Align center": "Alinear al centro",
    "Header 5": "Encabezado 5 ",
    "Heading 6": "Encabezado 6",
    "Heading 3": "Encabezado 3",
    "Decrease indent": "Disminuir sangr\u00eda",
    "Header 4": "Encabezado 4",
    "Paste is now in plain text mode. Contents will now be pasted as plain text until you toggle this option off.": "Pegar est\u00e1 ahora en modo de texto plano. El contenido se pegar\u00e1 como texto plano hasta que desactive esta opci\u00f3n.",
    "Underline": "Subrayado",
    "Cancel": "Cancelar",
    "Justify": "Justificar",
    "Inline": "en l\u00ednea",
    "Copy": "Copiar",
    "Align left": "Alinear a la izquierda",
    "Visual aids": "Ayudas visuales",
    "Lower Greek": "Inferior Griega",
    "Square": "Cuadrado",
    "Default": "Por defecto",
    "Lower Alpha": "Inferior Alfa",
    "Circle": "C\u00edrculo",
    "Disc": "Disco",
    "Upper Alpha": "Superior Alfa",
    "Upper Roman": "Superior Romana",
    "Lower Roman": "Inferior Romana",
    "Name": "Nombre",
    "Anchor": "Ancla",
    "You have unsaved changes are you sure you want to navigate away?": "Tiene cambios sin guardar. \u00bfEst\u00e1 seguro de que quiere salir?",
    "Restore last draft": "Restaurar el \u00faltimo borrador",
    "Special character": "Car\u00e1cter especial",
    "Source code": "C\u00f3digo fuente",
    "B": "A",
    "R": "R",
    "G": "V",
    "Color": "Color",
    "Right to left": "De derecha a izquierda",
    "Left to right": "De izquierda a derecha",
    "Emoticons": "Emoticonos",
    "Robots": "Robots",
    "Document properties": "Propiedades del documento",
    "Title": "T\u00edtulo",
    "Keywords": "Palabras clave",
    "Encoding": "Codificaci\u00f3n",
    "Description": "Descripci\u00f3n",
    "Author": "Autor",
    "Fullscreen": "Pantalla completa",
    "Horizontal line": "L\u00ednea horizontal",
    "Horizontal space": "Espacio horizontal",
    "Insert\/edit image": "Insertar\/editar imagen",
    "General": "General",
    "Advanced": "Avanzado",
    "Source": "Enlace",
    "Border": "Borde",
    "Constrain proportions": "Restringir proporciones",
    "Vertical space": "Espacio vertical",
    "Image description": "Descripci\u00f3n de la imagen",
    "Style": "Estilo",
    "Dimensions": "Dimensiones",
    "Insert image": "Insertar imagen",
    "Zoom in": "Acercar",
    "Contrast": "Contraste",
    "Back": "Atr\u00e1s",
    "Gamma": "Gamma",
    "Flip horizontally": "Invertir horizontalmente",
    "Resize": "Redimensionar",
    "Sharpen": "Forma",
    "Zoom out": "Alejar",
    "Image options": "Opciones de imagen",
    "Apply": "Aplicar",
    "Brightness": "Brillo",
    "Rotate clockwise": "Girar a la derecha",
    "Rotate counterclockwise": "Girar a la izquierda",
    "Edit image": "Editar imagen",
    "Color levels": "Niveles de color",
    "Crop": "Recortar",
    "Orientation": "Orientaci\u00f3n",
    "Flip vertically": "Invertir verticalmente",
    "Invert": "Invertir",
    "Insert date\/time": "Insertar fecha\/hora",
    "Remove link": "Quitar enlace",
    "Url": "URL",
    "Text to display": "Texto para mostrar",
    "Anchors": "Anclas",
    "Insert link": "Insertar enlace",
    "New window": "Nueva ventana",
    "None": "Ninguno",
    "The URL you entered seems to be an external link. Do you want to add the required http:\/\/ prefix?": "El enlace que has introducido no parece ser una enlace externo. Quieres a\u00f1adir el prefijo necesario http:\/\/ ?",
    "Target": "Destino",
    "The URL you entered seems to be an email address. Do you want to add the required mailto: prefix?": "El enlace que has introducido no parece ser una direcci\u00f3n de correo electr\u00f3nico. Quieres a\u00f1adir el prefijo necesario mailto: ?",
    "Insert\/edit link": "Insertar\/editar enlace",
    "Insert\/edit video": "Insertar\/editar video",
    "Poster": "Miniatura",
    "Alternative source": "Enlace alternativo",
    "Paste your embed code below:": "Pega tu c\u00f3digo embebido debajo",
    "Insert video": "Insertar video",
    "Embed": "Incrustado",
    "Nonbreaking space": "Espacio fijo",
    "Page break": "Salto de p\u00e1gina",
    "Paste as text": "Pegar como texto",
    "Preview": "Previsualizar",
    "Print": "Imprimir",
    "Save": "Guardar",
    "Could not find the specified string.": "No se encuentra la cadena de texto especificada",
    "Replace": "Reemplazar",
    "Next": "Siguiente",
    "Whole words": "Palabras completas",
    "Find and replace": "Buscar y reemplazar",
    "Replace with": "Reemplazar con",
    "Find": "Buscar",
    "Replace all": "Reemplazar todo",
    "Match case": "Coincidencia exacta",
    "Prev": "Anterior",
    "Spellcheck": "Corrector ortogr\u00e1fico",
    "Finish": "Finalizar",
    "Ignore all": "Ignorar todos",
    "Ignore": "Ignorar",
    "Add to Dictionary": "A\u00f1adir al Diccionario",
    "Insert row before": "Insertar fila antes",
    "Rows": "Filas",
    "Height": "Alto",
    "Paste row after": "Pegar la fila despu\u00e9s",
    "Alignment": "Alineaci\u00f3n",
    "Border color": "Color del borde",
    "Column group": "Grupo de columnas",
    "Row": "Fila",
    "Insert column before": "Insertar columna antes",
    "Split cell": "Dividir celdas",
    "Cell padding": "Relleno de celda",
    "Cell spacing": "Espacio entre celdas",
    "Row type": "Tipo de fila",
    "Insert table": "Insertar tabla",
    "Body": "Cuerpo",
    "Caption": "Subt\u00edtulo",
    "Footer": "Pie de p\u00e1gina",
    "Delete row": "Eliminar fila",
    "Paste row before": "Pegar la fila antes",
    "Scope": "\u00c1mbito",
    "Delete table": "Eliminar tabla",
    "H Align": "Alineamiento Horizontal",
    "Top": "Arriba",
    "Header cell": "Celda de la cebecera",
    "Column": "Columna",
    "Row group": "Grupo de filas",
    "Cell": "Celda",
    "Middle": "Centro",
    "Cell type": "Tipo de celda",
    "Copy row": "Copiar fila",
    "Row properties": "Propiedades de la fila",
    "Table properties": "Propiedades de la tabla",
    "Bottom": "Abajo",
    "V Align": "Alineamiento Vertical",
    "Header": "Cabecera",
    "Right": "Derecha",
    "Insert column after": "Insertar columna despu\u00e9s",
    "Cols": "Columnas",
    "Insert row after": "Insertar fila despu\u00e9s ",
    "Width": "Ancho",
    "Cell properties": "Propiedades de la celda",
    "Left": "Izquierda",
    "Cut row": "Cortar fila",
    "Delete column": "Eliminar columna",
    "Center": "Centrado",
    "Merge cells": "Combinar celdas",
    "Insert template": "Insertar plantilla",
    "Templates": "Plantillas",
    "Background color": "Color de fondo",
    "Custom...": "Personalizar...",
    "Custom color": "Color personalizado",
    "No color": "Sin color",
    "Text color": "Color del texto",
    "Show blocks": "Mostrar bloques",
    "Show invisible characters": "Mostrar caracteres invisibles",
    "Words: {0}": "Palabras: {0}",
    "Insert": "Insertar",
    "File": "Archivo",
    "Edit": "Editar",
    "Rich Text Area. Press ALT-F9 for menu. Press ALT-F10 for toolbar. Press ALT-0 for help": "\u00c1rea de texto enriquecido. Pulse ALT-F9 para el menu. Pulse ALT-F10 para la barra de herramientas. Pulse ALT-0 para ayuda",
    "Tools": "Herramientas",
    "View": "Ver",
    "Table": "Tabla",
    "Format": "Formato"
    });

    tinymce.init({
      selector: '#tinymceEditor',
      height: 1000,
      width: 892,
      theme: 'modern',
      plugins: [
        " customClose printwkhtmlpdf  importcss  pagebreak  contentDataModalComplex  advlist autolink lists link image charmap print preview hr anchor", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu imagetools directionality", "emoticons template paste textcolor colorpicker textpattern code"
      ],
      toolbar1: " undo redo | formatselect | bold italic |  alignleft aligncenter alignright alignjustify | bullist numlist outdent indent  | link image | pagebreak contentDataModalComplex | customClose ",
      // toolbar1: "print preview | insertfile undo redo  | bold italic fontsizeselect  fontselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent pagebreak | link image | code | help  | contentData contentDataModal contentDataModalComplex | previewContent print2 jsPDF jsPDFcanvas | printdompdf printwkhtmlpdf",
      file_picker_types: 'file image media',
      /*
      Incase the image "src" has to be replaced with the remote server upload path
      Image Upload handler should return a JSON {location:"absolute_path"}
      eg : {location:"http://cdnserver/sampleimage.png"}
      */
      images_upload_handler: function(blobInfo, success, failure) {
        var formData;
        formData = new FormData();
        formData.append('upload', blobInfo.blob(), blobInfo.filename());
        request.post('https://apps.anfix.com/os/media/upload')
        // .set('Cookie','accessCode=XXXXXXX')
          .withCredentials().timeout(10000).send(formData). // query string
        end(function(err, res) {
          if (err != null) {
            failure('Error: ' + err.message);
            return;
          }
          if (res.statusCode != 200) {
            failure('HTTP Error: ' + res.statusText);
            return;
          }
          var responseData = JSON.parse(res.text);
          if (responseData.result != 0) {
            failure(responseData.errorList[0].text);
            return;
          }
          var URL = "https://apps.anfix.com/os/media/download?uid=" + responseData.outputData.Media[0].UID;
          success(URL)
        });
      },
      file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function() {
            var id = 'IMG' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);
            cb(blobInfo.blobUri(), {title: file.name});
          };
        };
        input.click();
      },
      // Turn automatic_uploads to "false" to not to upload the data to server
      automatic_uploads: true,
      content_css: "./styles.css",
      visual_table_class: 'tableTinyMCE',
      inline_styles: true,
      entity_encoding: 'raw',
      mode: "exact",
      pagebreak_seperator: '<div style="page-break-after: always;"></div>',
      theme_advanced_path: false,
      menubar: false,
      statusbar: false,
      closeFunc: this.props.closeBtn,
      jsonData: this.props.variableJSON,
      uploadPath: this.props.uploadPath,
      pdfUploadURL: this.props.pdfUploadURL,
      printCallback: this.props.printCallbackFunction,
      masters: this,
      request: request
    });
    this.setState({"editor": tinymce.get("tinymceEditor")})
    this.state.editor.on('init', () => {
      this.state.editor.setContent(this.props.contentHTML);
    });
  }

  removeEditor() {
    window.tinymce.remove(this.state.editor);
    this.setState({editor: null});
  }

  printwkhtml() {
    this.state.editor.execCommand('tinymcewkhtmlPrint')
  }

  render() {
    const {content, config, className} = this.props;
    var editor = this.state.editor

    return (
      <div className="WebContentEditor">
        <div className="tinymceAnfixContainer">
          <textarea style={{"visibility": "hidden"}} className="" id="tinymceEditor"></textarea>
        </div>
        <Footer printFunction={this.printwkhtml} saveTemplate={this.saveTemplate} overloadTemplate={this.overloadTemplate}></Footer>
        <Modal editor={editor} show={this.props.showModal} onClose={this.toggleModal} jsonData={this.props.variableJSON}></Modal>
      </div>
    );
  }
}
