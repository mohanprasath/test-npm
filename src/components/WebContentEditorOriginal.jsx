import React from 'react';
import Footer from './Footer.jsx'
import VariableModal from './VariableModal.jsx'
var request = require('superagent');

class App extends React.Component {
  componentDidMount() {
    if (typeof window === 'undefined' || typeof document === 'undefined') return;
    if (window.tinymce || window._has_requested_tinymce) {
      this.initialiseEditor();
      return;
    }
    this.script = document.createElement('script');
    this.script.type = 'application/javascript';
    this.script.addEventListener('load', this.initialiseEditor);
    this.script.src = `https://cloud.tinymce.com/stable/tinymce.min.js`;
    document.head.appendChild(this.script);

    window._has_requested_tinymce = true;
  }
  initialiseEditor() {
    tinymce.init({
      selector: '#tinymceEditor',
      height: 1000,
      width: 892,
      theme: 'modern',
      plugins: [
        " customClose printwkhtmlpdf  importcss  pagebreak  contentDataModalComplex  advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu imagetools directionality", "emoticons template paste textcolor colorpicker textpattern code"
      ],
      toolbar1: " undo redo | formatselect | bold italic |  alignleft aligncenter alignright alignjustify | bullist numlist outdent indent  | link image | pagebreak contentDataModalComplex | customClose ",
      // toolbar1: "print preview | insertfile undo redo  | bold italic fontsizeselect  fontselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent pagebreak | link image | code | help  | contentData contentDataModal contentDataModalComplex | previewContent print2 jsPDF jsPDFcanvas | printdompdf printwkhtmlpdf",
      file_picker_types: 'file image media',
      /*
      Incase the image "src" has to be replaced with the remote server upload path
      Image Upload handler should return a JSON {location:"absolute_path"}
      eg : {location:"http://cdnserver/sampleimage.png"}
      */
      images_upload_handler: function(blobInfo, success, failure) {
        var formData;
        formData = new FormData();
        formData.append('upload', blobInfo.blob(), blobInfo.filename());
        request.post('https://apps.anfix.com/os/media/upload')
        // .set('Cookie','accessCode=XXXXXXX')
          .withCredentials().timeout(10000).send(formData). // query string
        end(function(err, res) {
          if (err != null) {
            failure('Error: ' + err.message);
            return;
          }
          if (res.statusCode != 200) {
            failure('HTTP Error: ' + res.statusText);
            return;
          }
          var responseData = JSON.parse(res.text);
          if (responseData.result != 0) {
            failure(responseData.errorList[0].text);
            return;
          }
          var URL = "https://apps.anfix.com/os/media/download?uid=" + responseData.outputData.Media[0].UID;
          success(URL)
        });
      },
      file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function() {
            var id = 'IMG' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);
            cb(blobInfo.blobUri(), {title: file.name});
          };
        };
        input.click();
      },
      // Turn automatic_uploads to "false" to not to upload the data to server
      automatic_uploads: true,
      content_css: "./styles.css",
      visual_table_class: 'tableTinyMCE',
      inline_styles: true,
      entity_encoding: 'raw',
      mode: "exact",
      theme_advanced_path: false,
      menubar: false,
      statusbar: false,
      closeFunc: this.props.closeBtn,
      jsonData: this.props.variableJSON,
      uploadPath: this.props.uploadPath,
      printCallback : this.props.printCallbackFunction
    });
    }
  printwkhtml() {
    tinymce.get("tinymceEditor").execCommand('tinymcewkhtmlPrint')
  }
  render() {
    return (
      <div className="WebContentEditor">
        <div className="tinymceAnfixContainer">
          <textarea className="invisisble" id="tinymceEditor">{this.props.contentHTML}</textarea>
        </div>
        <Footer printFunction={this.printwkhtml} saveTemplate={this.props.saveTemplate} soBringTemplate={this.props.soBringTemplate}></Footer>
        <VariableModal></VariableModal>
      </div>
    );
  }
}

export default App;
