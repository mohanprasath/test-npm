import React from 'react';
var request = require('superagent');

class App extends React.Component {
  componentDidMount() {
    tinymce.init({
      selector: '#tinymce',
      height: 500,
      theme: 'modern',
      file_picker_types: 'file image media',
      images_upload_handler: function(blobInfo, success, failure) {
      /*  var xhr,formData;
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'https://apps.anfix.com/os/media/upload');
        xhr.onload = function() {
          var json;
          if (xhr.status != 200) {
            failure('HTTP Error: ' + xhr.status);
            return;
          }

          json = JSON.parse(xhr.responseText);

          if (!json || typeof json.location != 'string') {
            failure('Invalid JSON: ' + xhr.responseText);
            return;
          }
          success(json.location);
        };
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
//         xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
// xhr.setRequestHeader("Access-Control-Allow-Credentials", " true");
        xhr.setRequestHeader("accessCode", "XMhUa.ajk");
        xhr.send(formData); */
        var formData;
        formData = new FormData();
        formData.append('upload', blobInfo.blob(), blobInfo.filename());
        request
          .post('https://apps.anfix.com/os/media/upload')
          // .set('Cookie','accessCode=XMzO3GiOo')
          .withCredentials()
          // .set('Content-Type', 'multipart/form-data')
          .send(formData) // query string
          .end(function(err, res){
            var responseData = JSON.parse(res.text);
            console.log(res)
            var URL = "https://apps.anfix.com/os/media/download?uid="+responseData.outputData.Media[0].UID;
            success(URL)
          });
      },
      file_picker_callback: function(cb, value, meta) {
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');
        input.onchange = function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function() {
            var id = 'IMG' + (new Date()).getTime();
            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
            var base64 = reader.result.split(',')[1];
            var blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);
            cb(blobInfo.blobUri(), {title: file.name});
          };
        };
        input.click();
      },
      automatic_uploads: true,
      plugins: [
        'advlist autolink lists link image charmap print preview hr anchor pagebreak', 'searchreplace wordcount visualblocks visualchars code fullscreen', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help code'
      ],
      toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
      toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help code',
    });
  }
  render() {
    return (
      <div>
        <textarea id="tinymce">Intssial losadser</textarea>
      </div>
    );
  }
}

export default App;
